var urlApiBotman = "http://localhost:8000/botman";
var urlApiHistorico = "http://localhost:8000/api/userHistory";
// var userId = +(new Date());
var userId = "";
var headers = {
    'Access-Control-Allow-Origin': '*'
};

var $chat_body = $("#chat_body");
var $input_send = $("#chatSend");
let scrollerId;
const $widget = document.querySelector('#prime');
const $chatScroll = document.querySelector('#chat_body');
let element = $chatScroll;
let $burbuja = $("#saludo-burbuja");


$(document).ready(function() {
    $burbuja.fadeIn(3000);
    userId = getCookie('newtonBotUuid');
    if (userId != "") {
        $.ajax({
            method: 'GET',
            url: urlApiHistorico + "/" + userId,
            headers: headers,
            processData: false,
            contentType: false,
        }).done(function(response) {
            if (response.data.length > 0) {
                continuar();
                // var messages = JSON.parse(response.data[0].UserChatHistory);
                // messages.forEach(message => {
                //     if (message.bot) {
                //         var img_avatar = document.createElement('img');
                //         $(img_avatar)
                //             .attr('src', './svg/Newton/Avatar.svg');
                //         var div_avatar = document.createElement('div');
                //         $(div_avatar)
                //             .addClass('chat_avatar')
                //             .append(img_avatar)
                //         var span = document.createElement('span');
                //         $(span)
                //             .addClass('chat_msg_item chat_msg_item_admin')
                //             .append(div_avatar, message.bot)

                //         $chat_body.append(span);
                //     } else {
                //         if (message.user != "inicio") {
                //             var span = document.createElement('span');
                //             $(span)
                //                 .addClass('chat_msg_item chat_msg_item_user')
                //                 .append(message.user)
                //                 .appendTo($chat_body);
                //         }
                //     }
                // });
                // callAPI('inicio');
            } else {
                userId = uuidv4();
                setCookie('newtonBotUuid', userId, 1 / 24);
                callAPI('inicio')
            }
        });
    } else {
        userId = uuidv4();
        setCookie('newtonBotUuid', userId, 1 / 24);
        callAPI('inicio')
    }



    // if (localStorage.getItem('userId')) {
    //     userId = localStorage.getItem('userId');
    //     callAPI('', false);
    // } else {
    //     localStorage.setItem('userId', userId);
    //     callAPI('inicio')
    // }



    $("#fab_send").on('click', function() {
        sendMessage();
    });
    $input_send.on('keyup', function(ev) {
        var keyCode = ev.which || ev.keyCode;
        if (keyCode == 13) {
            sendMessage();
        }
    });

    $("#fab_refresh").on('click', () => {

        $chat_body.empty();
        $input_send.hide();
        //callAPI('inicio');

    });


});

function continuar() {

    var img_avatar = document.createElement('img');
    $(img_avatar)
        .attr('src', './svg/Newton/Avatar.svg');
    var div_avatar = document.createElement('div');
    $(div_avatar)
        .addClass('chat_avatar')
        .append(img_avatar);
    var span = document.createElement('span');
    $(span)
        .addClass('chat_msg_item chat_msg_item_admin')
        .append(div_avatar, "¡Mira quien ha regresado! ¿Deseas continuar donde nos quedamos?")
        .appendTo($chat_body);

    var listActions = document.createElement('ul');
    $(listActions).addClass('picker')

    var liYes = document.createElement('li');
    $(liYes)
        .append('Si')
        .val(true)
        .addClass('item')
        .appendTo(listActions)
        .on('click', function() {
            $(liYes).parent().hide();
            accionContinuar(liYes.value);
        })
    var liNo = document.createElement('li');
    $(liNo)
        .append('No')
        .val(false)
        .addClass('item')
        .appendTo(listActions)
        .on('click', function() {
            $(liNo).parent().hide();
            accionContinuar(liNo.value)
        })

    var span = document.createElement('span');
    $(span)
        .addClass('tags');
    $(span)
        .addClass('chat_msg_item')
        .append(listActions)
        .appendTo($chat_body);

}


function accionContinuar(valor) {
    let estado = valor
    console.log("Valor de estado");
    console.log(estado);
    if (estado) {
        $.ajax({
            method: 'GET',
            url: urlApiHistorico + "/" + userId,
            headers: headers,
            processData: false,
            contentType: false,
        }).done(function(response) {
            if (response.data.length > 0) {
                var messages = JSON.parse(response.data[0].UserChatHistory);

                messages.forEach(message => {
                    console.log(messages.bot);
                    if (message.bot) {
                        var img_avatar = document.createElement('img');
                        $(img_avatar)
                            .attr('src', './svg/Newton/Avatar.svg');
                        var div_avatar = document.createElement('div');
                        $(div_avatar)
                            .addClass('chat_avatar')
                            .append(img_avatar)
                        var span = document.createElement('span');
                        $(span)
                            .addClass('chat_msg_item chat_msg_item_admin')
                            .append(div_avatar, message.bot)

                        $chat_body.append(span);
                    } else {
                        if (message.user != "inicio") {
                            var span = document.createElement('span');
                            $(span)
                                .addClass('chat_msg_item chat_msg_item_user')
                                .append(message.user)
                                .appendTo($chat_body);
                        }
                    }
                });

            }
        });
        callAPI('inicio');
    } else {

        userId = uuidv4();
        setCookie('newtonBotUuid', userId, 1 / 24);
        callAPI('inicio')

    }
}

function starScroll() {

    if (element.offsetHeight + element.scrollTop >= element.scrollHeight) {

        console.log("Estamos en el final del elemento");
        stopScroll();

    } else {

        console.log("No estamos en el final del elemento hay que dar scroll");
        scrollerId = setInterval(function() {
            $chatScroll.scrollBy(0, $chatScroll.scrollHeight);
            if (element.offsetHeight + element.scrollTop >= element.scrollHeight) {

                stopScroll();
            }

        }, 10);
    }
}

function stopScroll() {

    clearInterval(scrollerId);

}


// $form.addEventListener('submit', upload);
$widget.addEventListener('click', starScroll);

$('#prime').click(function() {
    toggleFab();
    if ($('#prime').attr("class") == "fab is-float is-visible") {
        $('#prime').empty();
        let closeChat = document.createElement('i');
        $(closeChat).addClass('prime zmdi zmdi-close is-active is-visible');
        $('#prime').append(closeChat);
        $('#saludo-burbuja').hide();
    } else {
        $('#prime').empty();
        let avatarChat = document.createElement('img');
        $(avatarChat).attr('src', './svg/Newton/Avatar.svg');
        $(avatarChat).attr('id', 'avatarNewton');
        $('#prime').append(avatarChat)
        $('#saludo-burbuja').show();

    }

});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    console.log(d.getTime());
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    console.log(d.getTime());
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


function callAPI(text, interactive = true, attachment = null, callback = null) {
    var data = new FormData();
    var postData = {
        driver: 'web',
        userId: userId,
        message: text,
        attachment,
        interactive,
    };
    Object.keys(postData).forEach(key => data.append(key, postData[key]));
    $.ajax({
        method: 'POST',
        url: urlApiBotman,
        headers: headers,
        data: data,
        processData: false,
        contentType: false,
    }).done(function(response) {
        var messages = response.messages || [];
        //console.log(messages);
        messages.forEach(message => {

            var img_avatar = document.createElement('img');
            $(img_avatar)
                .attr('src', './svg/Newton/Avatar.svg');
            var div_avatar = document.createElement('div');
            $(div_avatar)
                .addClass('chat_avatar')
                .append(img_avatar)
            if (message.additionalParameters == 'contacto') {

                var listActions = document.createElement('ul');
                var li = document.createElement('li');
                $(li).append(message.text)
                    .appendTo(listActions);
                $(listActions).addClass('tags');
                var spanActions = document.createElement('span');
                $(spanActions).addClass('chat_msg_item')
                    .append(listActions)
                    .appendTo($chat_body);

            } else {
                var span = document.createElement('span');
                $(span)
                    .addClass('chat_msg_item chat_msg_item_admin')
                    .append(div_avatar, message.text)

                $chat_body.append(span);

            }

            $('#chatSend').show();
            switch (message.type) {
                case "actions":
                    $('#chatSend').hide();
                    if (message.actions.length > 0) {
                        var listActions = document.createElement('ul');
                        message.actions.forEach(action => {
                            var li = document.createElement('li');
                            $(li)
                                .append(action.text)
                                .appendTo(listActions)
                                .on('click', function() {
                                    $(li).parent().hide();
                                    performAction(action.value, action.text);
                                });
                        });
                        $(listActions)
                            .addClass('tags');
                        var spanActions = document.createElement('span');
                        $(spanActions)
                            .addClass('chat_msg_item')
                            .append(listActions)
                            .appendTo($chat_body);
                    }

                    break;

                default:
                    break;
            }
            if (message.attachment) {
                switch (message.attachment.type) {
                    case "image":
                        var img_attach = document.createElement('img');
                        $(img_attach)
                            .attr("width", "100%")
                            .attr("src", message.attachment.url)
                            .appendTo(span);
                        break;

                    default:
                        break;
                }
            }

        });
        if (callback) {
            callback(response);
        }
        starScroll();
    })
}

function sendMessage() {
    var messageText = $input_send.val();
    $input_send.val('');
    messageText = messageText.trim();
    let text = $input_send;
    if (messageText.length > 0) {
        var span = document.createElement('span');
        $(span)
            .addClass('chat_msg_item chat_msg_item_user')
            .append(messageText)
            .appendTo($chat_body);
        callAPI(messageText);
    }

}

function performAction(value, text) {
    var span = document.createElement('span');
    $(span)
        .addClass('chat_msg_item chat_msg_item_user')
        .append(text)
        .appendTo($chat_body);
    callAPI(value);
}


// function upload(e) {
//     e.preventDefault();
//     console.log("Se dio click enviar archivo");
//     const formData = new FormData($form);
//     let archivo = formData.get('file');
//     console.log(archivo);
// }

//Toggle chat and links
function toggleFab() {
    $('.prime').toggleClass('zmdi-comment-outline');
    $('.prime').toggleClass('zmdi-close');
    $('.prime').toggleClass('is-active');
    $('#saludo-burbuja, .prime').toggleClass('is-visible');
    $('#prime').toggleClass('is-float');
    $('.chat').toggleClass('is-visible');
    $('.fab').toggleClass('is-visible');

}

$('#chat_first_screen').click(function(e) {
    hideChat(1);
});

$('#chat_second_screen').click(function(e) {
    hideChat(2);
});

$('#chat_third_screen').click(function(e) {
    hideChat(3);
});

$('#chat_fourth_screen').click(function(e) {
    hideChat(4);
});

$('#chat_fullscreen_loader').click(function(e) {
    $('.fullscreen').toggleClass('zmdi-window-maximize');
    $('.fullscreen').toggleClass('zmdi-window-restore');
    $('.chat').toggleClass('chat_fullscreen');
    $('.fab').toggleClass('is-hide');
    $('.header_img').toggleClass('change_img');
    $('.img_container').toggleClass('change_img');
    $('.chat_header').toggleClass('chat_header2');
    $('.fab_field').toggleClass('fab_field2');
    $('.chat_converse').toggleClass('chat_converse2');
    //$('#chat_converse').css('display', 'none');
    // $('#chat_body').css('display', 'none');
    // $('#chat_form').css('display', 'none');
    // $('.chat_login').css('display', 'none');
    // $('#chat_fullscreen').css('display', 'block');
});

function hideChat(hide) {
    switch (hide) {
        case 0:
            $('#chat_converse').css('display', 'none');
            $('#chat_body').css('display', 'none');
            $('#chat_form').css('display', 'none');
            $('.chat_login').css('display', 'block');
            $('.chat_fullscreen_loader').css('display', 'none');
            $('#chat_fullscreen').css('display', 'none');
            break;
        case 1:
            $('#chat_converse').css('display', 'block');
            $('#chat_body').css('display', 'none');
            $('#chat_form').css('display', 'none');
            $('.chat_login').css('display', 'none');
            $('.chat_fullscreen_loader').css('display', 'block');
            break;
        case 2:
            $('#chat_converse').css('display', 'none');
            $('#chat_body').css('display', 'block');
            $('#chat_form').css('display', 'none');
            $('.chat_login').css('display', 'none');
            $('.chat_fullscreen_loader').css('display', 'block');
            break;
        case 3:
            $('#chat_converse').css('display', 'none');
            $('#chat_body').css('display', 'none');
            $('#chat_form').css('display', 'block');
            $('.chat_login').css('display', 'none');
            $('.chat_fullscreen_loader').css('display', 'block');
            break;
        case 4:
            $('#chat_converse').css('display', 'none');
            $('#chat_body').css('display', 'none');
            $('#chat_form').css('display', 'none');
            $('.chat_login').css('display', 'none');
            $('.chat_fullscreen_loader').css('display', 'block');
            $('#chat_fullscreen').css('display', 'block');
            break;

    }
}